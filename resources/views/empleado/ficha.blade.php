@extends('layouts.admin')

@section('content')

<section class="content-header">
        <h1>
          
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Trabajador</a></li>
          <li class="active">Empleado</li>
        </ol>
      </section>

 <section class="content">
      
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
             <CENTER><b><i class="fa fa-users"></i> FICHA TRABAJADOR</b></CENTER>
          </div>
          <div class="box-body">
            
            <div class="col-md-6">
           <!--colms-->
         <div class="box box-default">
          <div class="box-header with-border text-center">
            <h3 class="box-title" >
             Datos Básicos del Empleado
           </h3>
          </div>
          <div class="box-body">


            <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group {{ $errors->has('cedula') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Cédula</label>
                  <input type="text" class="form-control" name="cedula" value="{{ old('cedula') }}">
                   <span class="help-block">{{ $errors->first('cedula') }}</span>
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group {{ $errors->has('nacionalidad') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nacionalidad</label>
                 <select class="form-control" id="nac" name="nacionalidad">
                  <option value="V">-Venezolano(a)-</option>
                  <option value="E">-Extranjero(a)-</option>
                   
                 </select>
                   <span class="help-block">{{ $errors->first('nacionalidad') }}</span>
                </div>
              </div>

            </div>
             <div class="col-md-12">              

              <div class="col-md-8">
                <div class="form-group {{ $errors->has('sexo') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Sexo</label>
                   <select class="form-control" id="sexo" name="sexo">
                  <option value="Masculino">-Masculino-</option>
                  <option value="Femenino">-Femenino-</option>
                   
                 </select>
                   <span class="help-block">{{ $errors->first('sexo') }}</span>
                </div>
              </div>

               <div class="col-md-4">
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Fecha de Nac.</label>
                  <input type="text" class="form-control text-uppercase datepicker" name="fecha" value="{{ old('nombre') }}" readonly="">
                   <span class="help-block">{{ $errors->first('fecha') }}</span>
                </div>
              </div>
              
            </div>

            <div class="col-md-12">
               <div class="col-md-6">
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Primer Nombre</label>
                  <input type="text" class="form-control text-uppercase" name="cedula" value="{{ old('nombre') }}">
                   <span class="help-block">{{ $errors->first('cedula') }}</span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Segundo Nombre</label>
                  <input type="text" class="form-control text-uppercase" name="cedula" value="{{ old('nombre') }}">
                   <span class="help-block">{{ $errors->first('cedula') }}</span>
                </div>
              </div>
            </div>

             <div class="col-md-12">
               <div class="col-md-6">
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Primer Apellido</label>
                  <input type="text" class="form-control text-uppercase" name="cedula" value="{{ old('nombre') }}">
                   <span class="help-block">{{ $errors->first('cedula') }}</span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Segundo Apellido</label>
                  <input type="text" class="form-control text-uppercase" name="cedula" value="{{ old('nombre') }}">
                   <span class="help-block">{{ $errors->first('cedula') }}</span>
                </div>
              </div>

            </div>

            

         

            <div class="col-md-12"> 

                <div class="col-md-12">                           
                <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }}">
                <label for="exampleInputPassword1">Domicilio Fiscal</label>
                <textarea class="form-control text-uppercase" name="direccion"></textarea>
                </div>
              </div>
            

              <div class="col-md-4">
                <div class="form-group {{ $errors->has('estado_civil') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Estado Civil</label>
                 <select name="estado_civil" class="form-control" required="">
                   <option value="Soltero(a)">Solter(a)</option>
                   <option value="Casado(a)">Casado(a)</option>
                   <option value="Divorciado(a)">Divorciado(a)</option>
                   <option value="Viudo(a)">Viudo(a)</option>
                 </select>
                   <span class="help-block">{{ $errors->first('estado_civil') }}</span>
                </div>
              </div>
    

              <div class="col-md-4">
                <div class="form-group {{ $errors->has('telefono_hab') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Teléfono Hab.</label>
                  <input type="text" class="form-control" name="telefono_hab" data-inputmask='"mask": "(9999) 999-9999"' data-mask value="{{ old('telefono_hab') }}">
                   <span class="help-block">{{ $errors->first('telefono_hab') }}</span>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group {{ $errors->has('telefono_movil') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Teléfono Mov.</label>
                  <input type="text" class="form-control" name="telefono_movil" data-inputmask='"mask": "(9999) 999-9999"' data-mask value="{{ old('telefono_movil') }}">
                   <span class="help-block">{{ $errors->first('telefono_movil') }}</span>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group {{ $errors->has('correo') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Correo Electrónico.</label>
                  <input type="text" class="form-control" name="correo" value="{{ old('correo') }}">
                   <span class="help-block">{{ $errors->first('correo') }}</span>
                </div>
              </div>

               <div class="col-md-12">
                <div class="form-group {{ $errors->has('profesion') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Profesion.</label>
                  <input type="text" class="form-control" name="profesion" value="{{ old('profesion') }}">
                   <span class="help-block">{{ $errors->first('profesion') }}</span>
                </div>
              </div>

             




              <div class="col-md-12">
                <div class="form-group {{ $errors->has('foto') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Foto Empleado.</label>
                  <input type="file"  name="foto" value="{{ old('foto') }}">
                   <span class="help-block">{{ $errors->first('foto') }}</span>
                </div>
              </div>
            
            </div>

          </div> 
        </div>

             <!--end col6-->
            </div>

             <div class="col-md-6">

               <div class="box box-default">
                <div class="box-header with-border text-center">
                  <h3 class="box-title" >
                    Complementos del Empleado
                  </h3>
                </div>
                  <div class="box-body">

                    <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa  fa-folder-open"></i> Ingreso</a></li>
              <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-cogs"></i> Datos de Abono</a></li>
              <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-check-square-o"></i> Concepto</a></li>
              <li><a href="#tab_4" data-toggle="tab"><i class="fa fa-users"></i> Carga Familiar</a></li>
             </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <center>
                <b>Datos Laborales del Empleado</b>
              </center>
              
              
              <div class="col-md-12">
                  <div class="col-md-12">
                <div class="form-group {{ $errors->has('grupo') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Tipo Trabajador.</label>
                  <select class="form-control" name="grupo" id="grupo">
                    
                  </select>
                   <span class="help-block">{{ $errors->first('grupo') }}</span>
                </div>
              </div>

                  <div class="col-md-6">
                <div class="form-group {{ $errors->has('grupo') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Grupo.</label>
                  <select class="form-control" name="grupo" id="grupo">
                    <option value="">-Seleccione-</option>
                  </select>
                   <span class="help-block">{{ $errors->first('grupo') }}</span>
                </div>
              </div>

               <div class="col-md-6">
                <div class="form-group {{ $errors->has('nivel') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nivel.</label>
                 <select class="form-control" name="nivel" id="nivel">
                  <option value="">-Seleccione-</option>
                </select>
                   <span class="help-block">{{ $errors->first('nivel') }}</span>
                </div>
              </div>


              <div class="col-md-4">
                <div class="form-group {{ $errors->has('fecha_ingreso') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Fecha Ingreso.</label>
                  <input type="text" class="form-control datepicker" name="fecha_ingreso" readonly="">
                
                   <span class="help-block">{{ $errors->first('fecha_ingreso') }}</span>
                </div>
              </div>

                <div class="col-md-4">
                <div class="form-group {{ $errors->has('fecha_ingreso') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Fecha Egreso.</label>
                    <input type="text" class="form-control datepicker" name="fecha_egreso" readonly="">
                
                   <span class="help-block">{{ $errors->first('fecha_egreso') }}</span>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group {{ $errors->has('fecha_vac') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Fecha  Vac.</label>
                  <input type="text" class="form-control datepicker" name="fecha_vac" readonly="">
                   <span class="help-block">{{ $errors->first('fecha_vac') }}</span>
                </div>
              </div>



              <div class="col-md-12">
                <div class="form-group {{ $errors->has('fecha_vac') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Dependencia.</label>
                  <select id="dep" class="form-control" name="dependencia_id">
                    <option value="">Seleccionar Dependencia</option>
                  
                  </select>
                   <span class="help-block">{{ $errors->first('fecha_vac') }}</span>
                </div>
              </div>

               <div class="col-md-12">
                <div class="form-group {{ $errors->has('fecha_vac') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Cargo Trabajador.</label>
                  <select id="dep" class="form-control" name="dependencia_id">
                    <option value="">Seleccionar Cargo</option>
                  
                  </select>
                   <span class="help-block">{{ $errors->first('fecha_vac') }}</span>
                </div>
              </div>
                
              </div>


               
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <center>
                <b>Abono de Nómina</b>

              </center>

              <hr>
              <div class="col-md-12">
                <div class="form-group {{ $errors->has('fecha_vac') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Tipo Pago.</label>
                  <select id="dep" class="form-control" name="dependencia_id">
                    <option value="">Seleccionar Cargo</option>
                  
                  </select>
                   <span class="help-block">{{ $errors->first('fecha_vac') }}</span>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group {{ $errors->has('banco') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Banco</label>
                  <select id="dep" class="form-control" name="cargo">
                    <option value="">Seleccionar Cargo</option>
                  
                  </select>
                   <span class="help-block">{{ $errors->first('bancofecha_vac') }}</span>
                </div>
              </div>

               <div class="col-md-6">
                <div class="form-group {{ $errors->has('tipo_cuenta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Tipo Cuenta</label>
                  <select id="dep" class="form-control" name="tipo_cuenta">
                    <option value="">Seleccionar Cargo</option>
                  
                  </select>
                   <span class="help-block">{{ $errors->first('tipo_cuenta') }}</span>
                </div>
              </div>

               <div class="col-md-6">
                <div class="form-group {{ $errors->has('nro_cuenta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Número Cuenta</label>
                  
                   <input type="text" class="form-control" data-inputmask='"mask": "99999999999999999999"' data-mask>
                   <span class="help-block">{{ $errors->first('nro_cuenta') }}</span>
                </div>
              </div>
               
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <center>
                <b>Conseptos Según Empleados</b>

                 <table class="table table-striped">



                 </table>
              </center>
          
              </div>

                <div class="tab-pane" id="tab_4">
                <center>
                <b>Carga Familiar Del Empleado</b>
                <br>
              

              <button class="btn btn-danger btn-xs"><i class="fa fa-plus"></i> Nueva Carga</button>
                </center>
                <div class="table-responsive">
               <table class="table table-striped">
               
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Cédula</th>
                      <th>Nombres Y Apellidos</th>
                      <th>Parentesco</th>
                      <th>Fecha Nac.</th>
                      <th></th>

                    </tr>                  
                  </thead>

                   <tr>
                      <td>1</td>
                      <td>v21464791</td>
                      <td>FELIPE FUENMAYOR YENDERSON GUZMÁN VALRO </td>
                      <td>Hijo</td>
                      <td>2018-01-01.</td>
                      <td></td>

                    </tr> 
               
                </table>
              </table>
          
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->


                  </div>

          <hr>
          <div class="col-md-12">

            <center>
              <button class="btn btn-primary"><i class="fa fa-check"></i>Ingresar Empleado</button>
              <button class="btn btn-default"><i class="fa fa-trash"></i>Limpiar</button>
            </center>
            
          </div>
              </div>
           
              
            </div>
          
           
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
</section>
@endsection
