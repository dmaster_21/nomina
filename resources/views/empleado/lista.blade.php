@extends('layouts.admin')

@section('content')

<section class="content-header">
        <h1>
          
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Trabajador</a></li>
          <li class="active">Listar Empleado</li>
        </ol>
      </section>

 <section class="content">
      
        <div class="box box-primary">
          <div class="box-header with-border text-center">
            <h3 class="box-title">
             <CENTER><b><i class="fa fa-users"></i> Listado de Trabajadores Activos</b></CENTER>
          </div>
          <div class="box-body">
            
            <div class="col-md-12">

              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Cédula Trabajador</th>
                  <th>Nombre Y Apellidos</th>
                  <th>Cargo</th>
                  <th>Dependencia</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                  <th>Cédula Trabajador</th>
                  <th>Nombre Y Apellidos</th>
                  <th>Cargo</th>
                  <th>Dependencia</th>
                  <th></th>
                </tr>
                
                <tfoot>
                </tfoot>
              </table>
       
              </div>
           
              
            </div>
          
           
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->


</section>
@endsection
