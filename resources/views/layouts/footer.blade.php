  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; {{ NOW()->format('Y') }} Web DesarrolladO Por El Personal de Sistemas Ingresado EN RRHH</strong> 
    </div>
    <!-- /.container -->
  </footer>