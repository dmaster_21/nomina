
@extends('layouts.login')
@section('content')
<div class="register-box box-primary" >
  <div class="register-logo">
    <a href="#"><b>Online</b>- Ecommerce</a>
  </div>

  <div class="register-box-body">

     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
    <center>
    <img src="{{ asset('portal/images/logo.png') }}" style="width: 40%" >
     <br>
    <h4><b>Cambiar  Contraseña</b></h4>
  </center>
    

     
      <div class="form-group has-feedback">
        <form method="POST" action="{{ route('password.update') }}" novalidate="">
                        @csrf
            <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
        <input type="email" class="form-control" name="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span class="help-block">{{ $errors->first('email') }}</span>
         </div>

         <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }} ">
        <input type="password" class="form-control" name="password" placeholder="Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password') }}</span>
      </div>

       

      <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}" >
        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
      </div>




     

                      

                        <div class="form-group row">
                          

                        <div class="form-group row mb-0">
                            <div class="col-md-12 ">
                                <center>
                                <button type="submit" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-log-in "></span>
                                    {{ __('Cambiar  Clave') }}
                                </button>
                                <a href="/" class="btn btn-default">
                                    <span class="glyphicon glyphicon-chevron-rigth "></span>                                
                                    {{ __('Volver') }}
                               
                                </a>

                            
                            </center>
                            </div>
                        </div>
                    </form>

       </div>
   

 

    


  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
@endsection
