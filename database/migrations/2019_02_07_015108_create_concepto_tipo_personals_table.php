<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConceptoTipoPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concepto_tipo_personals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tipo_empleados_id');
            $table->foreign('tipo_empleados_id')->references('id')->on('tipo_empleados');
            $table->unsignedInteger('conceptos_id');
            $table->foreign('conceptos_id')->references('id')->on('conceptos');
            $table->string('formula');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concepto_tipo_personals');
    }
}
