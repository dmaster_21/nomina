<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado_ingresos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empleado_id');
            $table->foreign('empleado_id')->references('id')->on('ficha_empleados');
            $table->unsignedInteger('tipo_empleado_id');
            $table->foreign('tipo_empleado_id')->references('id')->on('tipo_empleados');
            $table->Integer('grupo_id');
            $table->Integer('nivel_id');
            $table->timestamp('fecha_ingreso');
            $table->timestamp('fecha_egreso')->nullable();
            $table->timestamp('fecha_vacaciones');
            $table->Integer('dependencia_id');           
            $table->string('cargo');
            $table->integer('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleado_ingresos');
    }
}
