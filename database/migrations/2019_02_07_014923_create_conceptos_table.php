<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConceptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conceptos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->Integer('tipo_concepto_id');           
            $table->integer('fijo')->default('0');
            $table->integer('variable')->default('0');
            $table->integer('tabulador')->default('0');
            $table->integer('formulado')->default('0');
            $table->integer('pardita_id')->default('0');
            $table->integer('status')->default('1');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conceptos');
    }
}
