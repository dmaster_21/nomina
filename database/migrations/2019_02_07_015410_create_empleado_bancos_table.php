<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado_bancos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empleado_id');
            $table->foreign('empleado_id')->references('id')->on('ficha_empleados'); 
            $table->unsignedInteger('banco_id');
            $table->foreign('banco_id')->references('id')->on('bancos');

            $table->unsignedInteger('tipo_cobro_id');
            $table->foreign('tipo_cobro_id')->references('id')->on('tipo_cobros');

             $table->unsignedInteger('tipo_cuenta_id');
            $table->foreign('tipo_cuenta_id')->references('id')->on('tipo_cuentas');
            $table->string('nro_cuenta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleado_bancos');
    }
}
