## Sistema y gestion de nomina

gestion de nóminas

## Instalacion

Clonar Repositorio
```
git clone https://gitlab.com/dmaster_21/nomina.git
```

ejecutar Manejador de dependencias
```
composer install | composer update
```
ejecutar migraciones establecidas
```
php artisan migrate
```

Copiamos y reemplazamos el archivo .env.example de modo que generemos el archivo .env

Definimos parametros de conexion de Base de datos dentro del archivo .env
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

Finalmente Generar Clave laravel
```
php artisan key:generate
```



